## synonym:real
- reais
- R$
- pila
- pilas
- conto
- moeda brasileira

## synonym:dolar
- doletas
- dols
- moeda americana
- $

## synonym:euro
- euros
- €
- moeda europeia

## synonym:yen
- ¥
- yens

## synonym:bitcoin
- bitcoins
- cripto-moeda